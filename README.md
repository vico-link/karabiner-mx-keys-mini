# KARABINER-MX-KEYS-MINI

This json file contains the complex modifications for Karabiner-Elements in order to remap the keyboard in the following context.

## Pre-requisites

---

- install Karabiner-Elements (from here: https://karabiner-elements.pqrs.org/)

## Context

---

The keyboard model is the MX Keys Mini from Logitech in its French layout. The present configuration is for using it from a Mac and with a remote connection to a PC running MS Windows with MS Remote Desktop software.

This file has to be put into the following directory:

``` bash
.config/karabiner/assets/complex_modifications
```

## Mapped keys

- `-` (6 key) to `§`
- `_` (8 key) to `!`
- `=` to `-`
- `shift` + `=` to `_`
- `left_command` to `left_option`
